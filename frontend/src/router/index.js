import {createRouter, createWebHistory} from 'vue-router'
import Home from "@/views/Home.vue";
import SignUp from "@/views/SignUp.vue";
import LogIn from "@/views/LogIn.vue";
import MyAccount from "@/views/myAccount/MyAccount.vue";
import store from "@/store";
import Persons from "@/views/marriage/Persons.vue";
import Subsets from "@/views/marriage/Subsets.vue";
import Divisions from "@/views/fairDivision/Divisions.vue";
import DivisionDetails from "@/views/fairDivision/DivisionDetails.vue";

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home,
    },
    {
        path: '/sign-up',
        name: 'SignUp',
        component: SignUp,
    },
    {
        path: '/log-in',
        name: 'LogIn',
        component: LogIn,
    },
    {
        path: '/my-account',
        name: 'MyAccount',
        component: MyAccount,
        meta: {
            requireLogin: true
        }
    },
    {
        path: '/marriage/persons',
        name: 'Persons',
        component: Persons,
        meta: {
            requireLogin: true,
        }
    },
    {
        path: '/marriage/subsets',
        name: 'Subsets',
        component: Subsets,
        meta: {
            requireLogin: true,
        }
    },
    {
        path: '/fair-division/divisions',
        name: 'Divisions',
        component: Divisions,
        meta: {
            requireLogin: true,
        }
    },
    {
        path: '/fair-division/details',
        name: 'DivisionDetails',
        component: DivisionDetails,
        meta: {
            requireLogin: true,
        }
    },
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

router.beforeEach(
    (to, from, next) => {
        if (to.matched.some(record => record.meta.requireLogin) && !store.state.isAuthenticated) {
            next('log-in')
        } else {
            next()
        }
    }
)

export default router
