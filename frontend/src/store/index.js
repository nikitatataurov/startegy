import { createStore } from 'vuex'

export default createStore({
  state: {
    user: {
      username: ''
    },
    isAuthenticated: false,
    token: '',
  },
  getters: {
  },
  mutations: {
    initializeStore(state) {
      const token = localStorage.getItem('token')
      token ? this.setToken(state, token) : this.removeToken(state)
    },
    setToken(state, token) {
      state.token = token
      state.isAuthenticated = true
    },
    removeToken(state) {
      state.token = ''
      state.isAuthenticated = false
    },
  },
  actions: {
  },
  modules: {
  }
})

