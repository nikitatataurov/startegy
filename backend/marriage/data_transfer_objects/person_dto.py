class PersonDTO:
    """DTO for person"""
    def __init__(self, name: str, gender: str):
        self.name = name
        self.gender = gender
        self.preferences = {}
        self.engaged = None

    def add_preference(self, place: int, person):
        """Update person preferences dict"""
        self.preferences[place] = person

    def __str__(self):
        return self.name
