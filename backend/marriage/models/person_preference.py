from django.contrib.auth.models import User
from django.db import models

from .subset import Subset

from .person import Person


class PersonPreference(models.Model):
    """
    Model for person preference describing

    Who is the subject
    Whom is the object
    Describe: who wants whom and what is the place of the whom in preferences
    """
    who = models.ForeignKey(Person, related_name='preferences', on_delete=models.CASCADE)
    whom = models.ForeignKey(Person, related_name='in_preferences', on_delete=models.CASCADE)
    place = models.IntegerField(default=1)
    created_at = models.DateTimeField(auto_now_add=True)
    subset = models.ForeignKey(Subset, related_name='person_preferences', on_delete=models.CASCADE, blank=False, null=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['who', 'whom'], name='unique_person_preference'),
        ]

    def __str__(self):
        return f'{self.who.name} preferred {self.whom} on place: {self.place}'
