from django.contrib.auth.models import User
from django.db import models

from .subset import Subset

from ..value_objects.gender import Gender


class Person(models.Model):
    """Model for person describing"""
    name = models.CharField(max_length=77, blank=False, null=False)
    gender = models.CharField(max_length=1, choices=Gender.choices, default=Gender.MALE)
    created_at = models.DateTimeField(auto_now_add=True)
    subset = models.ForeignKey(Subset, related_name='persons', on_delete=models.CASCADE, blank=False, null=False)

    def __str__(self):
        return self.name
