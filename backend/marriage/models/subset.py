from django.contrib.auth.models import User
from django.db import models


class Subset(models.Model):
    """Model for subset of persons"""
    name = models.CharField(max_length=77, blank=False, null=False)
    created_by = models.ForeignKey(User, related_name='subsets', on_delete=models.CASCADE, blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name
