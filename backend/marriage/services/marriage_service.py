from ..data_transfer_objects import PersonDTO


class MarriageService:
    """Service for make stable matchmaking"""

    def make_marriage(self, males: list) -> list[PersonDTO]:
        """Make marriage between persons by Gale-Shapley algorithm"""
        free_male = True
        while free_male:
            free_male = False
            for male in males:
                if male.engaged is None:
                    for female in male.preferences.values():
                        if female.engaged is None:
                            male.engaged = female
                            female.engaged = male
                            break
                        else:
                            male_rating = 0
                            engaged_male_rating = 0
                            i = 0
                            for rated_male in female.preferences.values():
                                if rated_male is male:
                                    male_rating = i
                                if rated_male is female.engaged:
                                    engaged_male_rating = i
                                i += 1
                            if male_rating < engaged_male_rating:
                                female.engaged.engaged = None
                                female.engaged = male
                                male.engaged = female
                                break
            for male in males:
                if male.engaged is None:
                    free_male = True

        return males
