from rest_framework import serializers

from ..models import Subset


class SubsetSerializer(serializers.ModelSerializer):
    """Serializer for subset of persons"""

    class Meta:
        model = Subset
        read_only_fields = (
            'id',
            'created_by',
            'created_at',
        )
        fields = (
            'id',
            'name',
        )
