from rest_framework import serializers

from ..models import Person
from .person_preference_read_serializer import PersonPreferenceReadSerializer


class PersonSerializer(serializers.ModelSerializer):
    """Serializer for person"""
    preferences = PersonPreferenceReadSerializer(many=True, required=False)

    class Meta:
        model = Person
        fields = (
            'id',
            'name',
            'gender',
            'preferences',
            'subset',
        )
