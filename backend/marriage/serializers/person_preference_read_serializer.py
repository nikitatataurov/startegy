from rest_framework import serializers

from ..models import PersonPreference


class PersonPreferenceReadSerializer(serializers.ModelSerializer):
    """Serializer for person preference"""
    whom_name = serializers.StringRelatedField(source='whom')

    class Meta:
        model = PersonPreference
        fields = (
            'id',
            'whom_name',
            'place',
            'subset',
        )
