from rest_framework import serializers

from ..models import PersonPreference


class PersonPreferenceSerializer(serializers.ModelSerializer):
    """Serializer for person preference"""
    who_name = serializers.StringRelatedField(source='who.name')
    whom_name = serializers.StringRelatedField(source='whom.name')

    class Meta:
        model = PersonPreference
        fields = (
            'who_name',
            'whom_name',
            'place',
            'subset',
        )
