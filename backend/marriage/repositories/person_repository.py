from django.db.models import QuerySet

from ..models import Person, PersonPreference
from ..value_objects import Gender


class PersonRepository:
    """Repository for fetching persons"""

    def get_all_by_subset(self, subset: int) -> QuerySet[Person]:
        """Get subset persons"""
        return Person.objects.filter(subset=subset)

    def get_persons_by_subset(self, subset: int):
        """Get persons by subset"""
        return Person.objects.filter(subset=subset)

    def get_males(self, subset: int) -> QuerySet[Person]:
        """Get all male persons"""
        return self.get_persons_by_subset(subset=subset).filter(gender=Gender.MALE)

    def get_females(self, subset: int) -> QuerySet[Person]:
        """Get all female persons"""
        return self.get_persons_by_subset(subset=subset).filter(gender=Gender.FEMALE)

    def get_preferences_by_subset(self, subset: int) -> QuerySet[PersonPreference]:
        """Get preferences by subset"""
        return PersonPreference.objects.filter(subset=subset).order_by('place')
