from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response

from ..factories import PersonFactory
from ..models import Subset
from ..repositories import PersonRepository
from ..serializers import SubsetSerializer
from ..services import MarriageService


class SubsetViewSet(viewsets.ModelViewSet):
    """ViewSet for working with persons subsets"""
    serializer_class = SubsetSerializer

    def get_queryset(self):
        """Only creator can work with subset"""
        return Subset.objects.filter(created_by=self.request.user)

    def perform_create(self, serializer: SubsetSerializer):
        """Performing subset creating"""
        serializer.save(created_by=self.request.user)

    def perform_update(self, serializer: SubsetSerializer):
        """Performing subset updating"""
        if self.request.user != self.get_object().created_by:
            raise PermissionDenied('Incorrect object creator')

        serializer.save()

    @action(detail=True)
    def make(self, _, pk: int):
        """
        Make marriage and get pairs

        We don't need to duplicate pairs => take pairs basic only by one gender
        """
        if self.request.user != self.get_object().created_by:
            raise PermissionDenied('Incorrect subset creator')

        person_repository = PersonRepository()
        males = MarriageService().make_marriage(males=PersonFactory().get_males(
            persons=person_repository.get_all_by_subset(subset=pk),
            preferences=person_repository.get_preferences_by_subset(subset=pk)
        ))

        return Response([{'name': male.name, 'engaged_name': male.engaged.name} for male in males])
