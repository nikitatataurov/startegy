from rest_framework import viewsets
from rest_framework.exceptions import PermissionDenied

from ..models import PersonPreference, Person
from ..serializers import PersonPreferenceSerializer


class PersonPreferenceViewSet(viewsets.ModelViewSet):
    """ViewSet for work with PersonPreference"""
    serializer_class = PersonPreferenceSerializer

    def get_queryset(self):
        """Only creator can interact with person preference and opportunity to get by concrete person"""
        return PersonPreference.objects.filter(subset=self.request.GET.get('subset'))

    def perform_create(self, serializer: PersonPreferenceSerializer):
        """Performing person_preference saving"""
        serializer_data: dict = serializer.initial_data

        serializer.save(
            who=Person.objects.get(name=serializer_data['who_name'], subset=serializer_data['subset']),
            whom=Person.objects.get(name=serializer_data['whom_name'], subset=serializer_data['subset']),
        )

    def perform_update(self, serializer: PersonPreferenceSerializer):
        """Performing person_preference updating"""
        if self.request.user != self.get_object().subset.created_by:
            raise PermissionDenied('Incorrect object creator')
        serializer.save()
