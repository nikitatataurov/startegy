from rest_framework import viewsets
from rest_framework.exceptions import PermissionDenied

from ..models import Person, Subset
from ..serializers import PersonSerializer


class PersonViewSet(viewsets.ModelViewSet):
    """ViewSet for work with Person"""
    serializer_class = PersonSerializer

    def get_queryset(self):
        """Only creator can interact with person"""
        subset = self.request.GET.get('subset')
        if Subset.objects.get(id=subset).created_by != self.request.user:
            raise PermissionDenied('Incorrect person try to get access')
        return Person.objects.filter(subset=self.request.GET.get('subset'))
