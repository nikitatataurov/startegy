from django.utils.translation import gettext_lazy
from django.db.models import TextChoices


class Gender(TextChoices):
    """Value object for gender choices"""
    MALE = 'M', gettext_lazy('Male')
    FEMALE = 'F', gettext_lazy('Female')
