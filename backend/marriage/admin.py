from django.contrib import admin

from .models import Person, PersonPreference, Subset

admin.site.register(Person)
admin.site.register(PersonPreference)
admin.site.register(Subset)
