from rest_framework.routers import DefaultRouter

from .views import PersonViewSet, PersonPreferenceViewSet, SubsetViewSet

router: DefaultRouter = DefaultRouter()

router.register('persons', PersonViewSet, basename='persons')
router.register('person_preferences', PersonPreferenceViewSet, basename='person_preferences')
router.register('subsets', SubsetViewSet, basename='subsets')

urlpatterns = router.urls
