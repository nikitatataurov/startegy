from django.db.models import QuerySet

from ..data_transfer_objects import PersonDTO
from ..models import Person, PersonPreference
from ..value_objects import Gender


class PersonFactory:
    """Factory for making persons"""

    def get_males(self, persons: QuerySet[Person], preferences: QuerySet[PersonPreference]):
        """Making persons without preferences"""
        persons_dict = {}
        for person in persons:
            persons_dict[person.name] = PersonDTO(
                name=person.name,
                gender=person.gender,
            )

        for preference in preferences:
            persons_dict[preference.who.name].add_preference(
                place=preference.place,
                person=persons_dict[preference.whom.name]
            )

        return [person for person in persons_dict.values() if person.gender == Gender.MALE]
