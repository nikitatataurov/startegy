class FairDivisionService:
    """Service for making fair division"""

    def divide_fair(self, full_amount: int, wishes: list[int]) -> list:
        """Making fair division"""
        participants_count: int = len(wishes)
        wishes_on_layer: list = [0 for _ in range(participants_count)]
        result: list = [0 for _ in range(participants_count)]
        layers: int = 2
        layer: int = 1
        while full_amount > 0:
            if layer <= layers:
                min_wish: float = 0
                for participant_number in range(participants_count):
                    if (
                            wishes[participant_number] / layers - wishes_on_layer[participant_number] < min_wish
                            or min_wish == 0
                    ) and wishes_on_layer[participant_number] < wishes[participant_number] / layers:
                        min_wish = wishes[participant_number] / layers - wishes_on_layer[participant_number]
                satisfied: int = 0
                for participant_number in range(participants_count):
                    if wishes_on_layer[participant_number] == wishes[participant_number] / layers:
                        satisfied += 1
                if min_wish * (participants_count - satisfied) > full_amount:
                    min_wish = full_amount / (participants_count - satisfied)
                for participant_number in range(participants_count):
                    if wishes_on_layer[participant_number] != wishes[participant_number] / layers:
                        wishes_on_layer[participant_number] += min_wish
                        full_amount -= min_wish
                satisfied = 0
                for participant_number in range(participants_count):
                    if wishes_on_layer[participant_number] == wishes[participant_number] / layers:
                        satisfied += 1
                if satisfied == participants_count:
                    for participant_number in range(participants_count):
                        result[participant_number] += wishes_on_layer[participant_number]
                        wishes_on_layer[participant_number] = 0
                    layer += 1
            else:
                min_wish = full_amount / participants_count
                for participant_number in range(participants_count):
                    result[participant_number] += min_wish
                    full_amount -= min_wish
        for participant_number in range(participants_count):
            result[participant_number] += wishes_on_layer[participant_number]

        return result
