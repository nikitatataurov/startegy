from django.db import models

from .division import Division


class Wish(models.Model):
    """Model for describing wish"""
    amount = models.PositiveIntegerField()
    division = models.ForeignKey(Division, related_name='wishes', on_delete=models.CASCADE, blank=False, null=False)
    number = models.PositiveIntegerField()

    def __str__(self):
        return str(self.amount)
