from django.db import models

from .division import Division


class FullAmount(models.Model):
    """Model for describing full amount"""
    amount = models.IntegerField()
    division = models.OneToOneField(Division, related_name='full_amount', on_delete=models.CASCADE, blank=False, null=False)

    def __str__(self):
        return str(self.amount)
