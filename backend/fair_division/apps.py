from django.apps import AppConfig


class FairDivisionConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'fair_division'
