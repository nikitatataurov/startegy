from django.db.models import QuerySet

from ..models import Wish


class WishRepository:
    """Repository for fetching wishes"""

    def get_all_by_division_order_by_number(self, division: int) -> QuerySet[Wish]:
        """Get wishes for division"""
        return Wish.objects.filter(division=division).order_by('number')
