from ..models import FullAmount


class FullAmountRepository:
    """Repository for fetching full amounts"""

    def get_by_division(self, division: int) -> FullAmount:
        """Get full amount for division"""
        return FullAmount.objects.filter(division=division).get()
