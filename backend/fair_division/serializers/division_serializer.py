from rest_framework import serializers

from ..models import Division


class DivisionSerializer(serializers.ModelSerializer):
    """Serializer for division"""

    class Meta:
        model = Division
        fields = (
            'id',
            'name',
        )
