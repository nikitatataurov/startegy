from rest_framework import serializers

from ..models import Wish


class WishSerializer(serializers.ModelSerializer):
    """Serializer for wish"""

    class Meta:
        model = Wish
        fields = (
            'amount',
            'division',
            'number',
        )
