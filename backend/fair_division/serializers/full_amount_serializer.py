from rest_framework import serializers

from ..models import FullAmount


class FullAmountSerializer(serializers.ModelSerializer):
    """Serializer for full amount"""

    class Meta:
        model = FullAmount
        fields = (
            'id',
            'amount',
            'division',
        )
