from django.contrib import admin
from .models import FullAmount, Division, Wish

admin.site.register(FullAmount)
admin.site.register(Division)
admin.site.register(Wish)
