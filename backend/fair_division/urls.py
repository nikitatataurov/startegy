from rest_framework.routers import DefaultRouter

from .views import DivisionViewSet, FullAmountViewSet, WishViewSet

router = DefaultRouter()
router.register('divisions', DivisionViewSet, basename='divisions')
router.register('full-amount', FullAmountViewSet, basename='full_amount')
router.register('wishes', WishViewSet, basename='wishes')

urlpatterns = router.urls
