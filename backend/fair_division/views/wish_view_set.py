from rest_framework import viewsets
from rest_framework.exceptions import PermissionDenied

from ..models import Wish, Division
from ..serializers import WishSerializer


class WishViewSet(viewsets.ModelViewSet):
    """ViewSet for work with wish"""
    serializer_class = WishSerializer

    def get_queryset(self):
        """Only creator can interact with wish"""
        division_id = self.request.GET.get('division')
        if Division.objects.get(id=division_id).created_by != self.request.user:
            raise PermissionDenied('Incorrect wish try to get access')
        return Wish.objects.filter(division_id=self.request.GET.get('division'))
