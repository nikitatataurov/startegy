from rest_framework import viewsets
from rest_framework.exceptions import PermissionDenied

from ..models import FullAmount, Division
from ..serializers import FullAmountSerializer


class FullAmountViewSet(viewsets.ModelViewSet):
    """ViewSet for work with full amount"""
    serializer_class = FullAmountSerializer

    def get_queryset(self):
        """Only creator can interact with full amount"""
        division_id = self.request.GET.get('division')
        if Division.objects.get(id=division_id).created_by != self.request.user:
            raise PermissionDenied('Incorrect full amount try to get access')
        return FullAmount.objects.filter(division_id=self.request.GET.get('division'))
