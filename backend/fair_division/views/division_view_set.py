from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response

from ..repositories import FullAmountRepository, WishRepository
from ..serializers import DivisionSerializer
from ..services import FairDivisionService
from ..models import Division


class DivisionViewSet(viewsets.ModelViewSet):
    """ViewSet for work with division"""
    serializer_class = DivisionSerializer
    queryset = Division.objects.all()

    def get_queryset(self):
        """Only creator can work with division"""
        return Division.objects.filter(created_by=self.request.user)

    def perform_create(self, serializer: DivisionSerializer):
        """Performing division creating"""
        serializer.save(created_by=self.request.user)

    def perform_update(self, serializer: DivisionSerializer):
        """Performing division updating"""
        if self.request.user != self.get_object().created_by:
            raise PermissionDenied('Incorrect division creator')

        serializer.save()

    @action(detail=True)
    def make(self, _, pk: int):
        """Action for making division"""

        if self.request.user != self.get_object().created_by:
            raise PermissionDenied('Incorrect division creator')

        results = FairDivisionService().divide_fair(
            full_amount=FullAmountRepository().get_by_division(division=pk).amount,
            wishes=[wish.amount for wish in WishRepository().get_all_by_division_order_by_number(division=pk)]
        )
        return Response([{'number': number, 'amount': result} for number, result in enumerate(results, 1)])
